#include <stdio.h>
#include <stdlib.h>

#define medium(X) X*1.3
#define large(X) X*1.6

//Ohmics 1
void size_function();
int main()
{
  //Variables
    int order, comp, comp_b;
    float soda, coffee, milkV, milkC, hburg, cburg;
    float gburg, frenchf, spotato, hdog, dbburg;
    float total, tax;
    char user_add;
    int usr_swt;
    int size_swt;
    float obj_size;
    float tax_var;

    total = 0;
    //TAXES
    tax = .15;
    //Saving prices to food variables - Prices in $
    soda = 1;
    coffee = 1.5;
    milkV = 2;
    milkC = 2;
    hburg = 5.25;
    cburg = 5.75;
    gburg = 5.50;
    frenchf = 1.25;
    spotato = 3;
    hdog = 3.50;
    dbburg = 6.50;

     //Beginning Title Block
        printf("Welcome to Ohmics 1 Online Restaurant Simulator.\n\n");
        printf("On our menu, we have eleven items you might enjoy.\n");
        printf("Each of these items comes in three sizes, the price of the small size is shown on the menu.\n\n");
        printf("The price for a medium item is 1.3 times the price of the \nsmall item, and a large is 1.6 times the price of a small.\n");
     //End Title Block

     //Menu and stuff
        printf("Below is the menu\n\n\n");
        printf("To order, please type in the number designated for the item.\n");
        printf("   1-Soda                      2-Coffee           3-Milkshake, Vanilla\n");
        printf("   4-Milkshake, Chocolate      5-Hamburger        6-Cheeseburger\n");
        printf("   7-Gardenburger              8-French Fries     9-Sweet Potato\n");
        printf("   10-Hot Dog                  11-Double-Bacon Burger\n");
        printf("   12-Total & Cash out\n");
     //End Menu

    printf("Please enter your desired item.\n   ");
    do
    {
        do{
            scanf("%d", &order);
            //Beginning of massive code cluster thing
                switch(order)
                {
                    case 1:
                    comp = 0; // write our variable to FALSE for the loop

                        printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = soda;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(soda);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(soda);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = soda;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 2:
                        comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = coffee;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(coffee);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(coffee);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = coffee;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 3:
                    comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = milkV;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(milkV);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(milkV);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = milkV;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 4:
                    comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = milkC;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(milkC);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(milkC);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = milkC;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 5:
                    comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = hburg;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(hburg);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(hburg);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = hburg;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 6:
                    comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = cburg;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(cburg);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(cburg);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = cburg;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 7:
                    comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = gburg;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(gburg);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(gburg);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = gburg;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 8:
                    comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = frenchf;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(frenchf);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(frenchf);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = frenchf;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 9:
                    comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = spotato;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(spotato);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(spotato);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = spotato;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 10:
                    comp = 0;
                        printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = hdog;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(hdog);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(hdog);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = hdog;
                                total = total + obj_size;
                                break;

                            }
                        break;
                    case 11:
                    comp = 0;
                         printf("What size would you like? 1] small 2] medium 3] large\n");
                        scanf("%d", &size_swt);
                        switch(size_swt)
                            {
                            case 1:
                                obj_size = dbburg;
                                total = total + obj_size;
                                break;

                            case 2:
                                obj_size = medium(dbburg);
                                total = total + obj_size;
                                break;
                            case 3:
                                obj_size = large(dbburg);
                                total = total + obj_size;
                                break;
                            default:
                                obj_size = dbburg;
                                total = total + obj_size;
                                break;

                            case 12:
                                comp = 0;
                                tax_var = total * tax;
                                total =total + (total * tax);
                                printf("Your total is %.2f (%.2f tax)", total, tax_var);
                                printf("\n Thank you for coming in!\n");
                                break;
                            }
                        break;
                    default:
                    comp = 1; // Write our variable TRUE for the loop.
                        printf("Not a valid option, please select again.\n");
                        break;

                }//End of switch case

          }//End of 1st do

    while(comp == 1);

    printf("Your total = $%.2f\n", total);
    printf("would you like to add anything else?\n Enter 'y' or 'n'\n");

    scanf("%s", &user_add);

    usr_swt = user_add;
    switch(usr_swt)
        {
            case 'y':
                comp_b = 1;
                printf("Please enter your other item\n");
                break;

            case 'n':
                comp_b = 0;
                break;

            default:
                comp_b = 0;
                break;
        }

    }
while(comp_b == 1);
tax_var = total * tax;
total =total + (total * tax);
printf("Your total is %.2f (%.2f tax)", total, tax_var);
printf("\n Thank you for coming in!\n");
}
